from .core import play, Human, Computer

__all__ = ('play', 'Human', 'Computer')
